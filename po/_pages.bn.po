# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Oymate <dhruboadittya96@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-06-24 23:12+0200\n"
"PO-Revision-Date: 2021-06-17 19:37+0000\n"
"Last-Translator: Oymate <dhruboadittya96@gmail.com>\n"
"Language-Team: Bengali <https://hosted.weblate.org/projects/f-droid/website-pages/bn/>\n"
"Language: bn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 4.7\n"

#. type: YAML Front Matter: title
#: _pages/403.md
#, no-wrap
msgid "403 Forbidden"
msgstr "৪০৩ নিষিদ্ধ"

#. type: Plain text
#: _pages/403.md _pages/404.md
msgid "If you think there should be something here, please [submit an issue](https://gitlab.com/fdroid/fdroid-website/issues)."
msgstr "যদি তুমি মনে করো কিছু করা উচিত, অনুগ্রহ করে এখানে [একটি ইস্যু](https://gitlab.com/fdroid/fdroid-website/issues) জমা দাও।"

#. type: YAML Front Matter: title
#: _pages/404.md
#, no-wrap
msgid "404 Page Not Found"
msgstr "৪০৪ পৃষ্ঠা পাওয়া যায়নি"

#. type: YAML Front Matter: title
#: _pages/about.md
#, no-wrap
msgid "About"
msgstr "আমাদের সম্পর্কে"

#. type: Plain text
#: _pages/about.md
msgid "F-Droid is a robot with a passion for Free and Open Source Software (FOSS) on the Android platform. On this site you’ll find a repository of FOSS apps, along with an Android client to perform installations and updates, news, reviews, and other features covering all things Android and software-freedom related."
msgstr "এফ-ড্রইড একটি রোবট যার অ্যান্ড্রয়েড প্ল্যাটফর্মে মুক্ত ও ওপেন সোর্স সফটওয়্যারের ব্যাপারে ভীষণ আগ্রহ। এই ওয়েবসাইটে তুমি মুক্ত অ্যাপের একটি ভাণ্ডার, এবং সঙ্গে অ্যান্ড্রয়েড ক্লায়েন্ট পাবে যার মাধ্যমে তুমি অ্যাপ ইন্সটল, হালনাগাদ করা, খবর, পর্যালোচনা এবং অন্যান্য বৈশিষ্ট্য এবং অ্যান্ড্রয়েড সফটওয়্যারে স্বাধীনতা নিয়ে সবকিছু জানতে পারবে।"

#. type: Plain text
#: _pages/about.md
msgid "Donations for F-Droid development are handled via our [OpenCollective](https://opencollective.com/F-Droid/).  Donations for the _f-droid.org_ core infrastructure are handled by F-Droid Limited, a UK \"private company\" registered in England (no. [08420676](https://beta.companieshouse.gov.uk/company/08420676))."
msgstr "[মুক্তসমন্বয়](https://opencollective.com/F-Droid/) দিয়ে অনুদানসমূহের ব্যবস্থাপনা করা হয়। _f-droid.org_ কেন্দ্রীয় পরিকাঠামোর জন্য অনুদানগুকো এফ-ড্রয়েড লিমিটেড, যুক্তরাজ্যের ইংল্যান্ডের একটি \"গোপনীয়তা প্রতিষ্ঠান\" হিসেবে নিবন্ধিত (নং. [০৮৪২০৬৭৬](https://beta.companieshouse.gov.uk/company/08420676))।"

#. type: Title ###
#: _pages/about.md
#, no-wrap
msgid "Contact"
msgstr "যোগাযোগ"

#. type: Plain text
#: _pages/about.md
msgid "You can discuss F-Droid or get help on the [forum](https://forum.f-droid.org) and F-Droid's user chat."
msgstr "এফ-ড্রয়েড নিয়ে কথা বলতে বা সাহায্য নিতে পারো [ফোরামে](https://forum.f-droid.org) বা এফ-ড্রয়েড ব্যবহারকারী আড্ডায়।"

#. type: Plain text
#: _pages/about.md
msgid "To chat with others about F-Droid, either join via IRC in the [#fdroid channel](https://webchat.oftc.net/?randomnick=1&channels=fdroid&prompt=1) on OFTC or join [#fdroid:f-droid.org](https://matrix.to/#/#fdroid:f-droid.org) on Matrix.  The discussions are logged on [matrix.f-droid.org](https://matrix.f-droid.org/alias/%23fdroid:f-droid.org)."
msgstr ""

#. type: Plain text
#: _pages/about.md
msgid "There are also bridges to Telegram and Jabber/XMPP (experimental).  To join via Telegram, click [this link](https://matrix.f-droid.org/fdroid/telegram/).  For Jabber/XMPP, join [#fdroid#matrix.org@bridge.xmpp.matrix.org](xmpp:#fdroid#matrix.org@bridge.xmpp.matrix.org?join)."
msgstr ""

#. type: Plain text
#: _pages/about.md
msgid "F-Droid contributors also run a social-media account on the Fediverse. You can interact with us or follow us on [@fdroidorg@mastodon.technology](https://mastodon.technology/@fdroidorg)."
msgstr ""

#. type: Plain text
#: _pages/about.md
msgid "You can send email at [team@f-droid.org](mailto:team@f-droid.org), but messages are much more likely to be answered on the forum or #fdroid channel. If you want to help, you can [join us]({{ \"/contribute/\" | prepend: site.baseurl }})."
msgstr "আমাদেরকে ইমেইলও করতে পারেন [team@f-droid.org](mailto:team@f-droid.org) ঠিকানায়, কিন্তু জবাব পাওয়ার সবচেয়ে বেশি সুযোগ থাকে ফোরামে(সভা) বা #fdroid চ্যানেলে। আপনি যদি এই প্রকল্পে কোনো সাহায্য করতে চান, তাহলে [এখানে যুক্ত হও]({{ \"/contribute/\" | prepend: site.baseurl }})।"

#. type: Title ###
#: _pages/about.md
#, no-wrap
msgid "Consulting / Commercial Support"
msgstr "পরামর্শ / বাণিজ্যিক সাপোর্ট"

#. type: Plain text
#: _pages/about.md
msgid "If you're running an organisation that uses F-Droid or would like to use F-Droid, you will sometimes want to have an expert. For this, we have a list of companies and developers who have contributed to F-Droid in one way or another and are available for consulting:"
msgstr ""

#. type: Plain text
#: _pages/about.md
msgid "[Find an F-Droid Consultant]({{ \"/consulting/\" | prepend: site.baseurl }})"
msgstr "[এফ-ড্রয়েড নিয়ে একজন পরামর্শক খুঁজো]({{ \"/consulting/\" | prepend: site.baseurl }})"

#. type: Title ###
#: _pages/about.md
#, no-wrap
msgid "License"
msgstr "অনুমতিপত্র"

#. type: Bullet: '* '
#: _pages/about.md
msgid "The source code for this site and the content produced by the F-Droid community are available under the [GNU Affero General Public License version 3 or later (AGPLv3+)](https://www.gnu.org/licenses/agpl-3.0.html)."
msgstr ""

#. type: Bullet: '* '
#: _pages/about.md
msgid "The content of the news section and the F-Droid logo are also available under [Creative Commons Attribution-ShareAlike 3.0 License (CC-BY-SA-3.0)](http://creativecommons.org/licenses/by-sa/3.0/) or [GNU General Public License version 3 or later (GPLv3+)](https://www.gnu.org/licenses/gpl-3.0.html)."
msgstr ""

#. type: Bullet: '* '
#: _pages/about.md
msgid "The descriptive text and graphics for each app have a specific license from that project.  Free software should also have free descriptions and graphics."
msgstr ""

#. type: Bullet: '* '
#: _pages/about.md
msgid "The F-Droid logo was created by William Theaker. Updates to that, and additional artwork, are by Robert Martinez."
msgstr ""

#. type: Title ###
#: _pages/about.md
#, no-wrap
msgid "Terms, etc."
msgstr "শর্তাবলী, ইত্যাদি"

#. type: Plain text
#: _pages/about.md
msgid "F-Droid is a non-profit [volunteer]({{ \"/contribute/\" | prepend: site.baseurl }}) project. Although every effort is made to ensure that everything in the repository is safe to install, you use it AT YOUR OWN RISK. Wherever possible, applications in the repository are built from source, and that source code is checked for potential security or privacy issues. This checking is far from exhaustive and there are no guarantees."
msgstr ""

#. type: Plain text
#: _pages/about.md
#, fuzzy
msgid "F-Droid respects your privacy. We don’t track you, or your device. We don’t track what you install. You don’t need an account to use the client, and it sends no additional identifying data when communicating with our web servers, other than its version number. We don’t even allow you to install other applications from the repository that track you, unless you first enable ‘Tracking’ in the `AntiFeatures` section of preferences. Any personal data you decide to give us (e.g. your email address when registering for an account to post on the forum) goes no further than us, and will not be used for anything other than allowing you to maintain your account."
msgstr "এফ-ড্রয়েড তোমার গোপনীয়তা সম্মান করে। আমরা তোমার বা তোমার ডিভাইসের উপর নজরদারী করি না। গ্রাহক অ্যাপ ব্যবহার করতে তোমার কোনো অ্যাকাউন্ট লাগবে না, আর কোনো"

#. type: Title ###
#: _pages/about.md
#, no-wrap
msgid "Contributors"
msgstr "অবদানকারী"

#. type: Plain text
#: _pages/about.md
msgid "The F-Droid project was founded in 2010 by Ciaran Gultnieks, and is brought to you by at least the following people:"
msgstr ""

#. type: Plain text
#: _pages/about.md
#, no-wrap
msgid ""
"<ul>\n"
"{%- for contributor in site.data.contributors -%}\n"
"<li>\n"
"{%- if contributor.gitlab -%}\n"
"<a href=\"https://gitlab.com/{{ contributor.gitlab }}\" target=\"_blank\">\n"
"{%- elsif contributor.weblate -%}\n"
"<a href=\"https://hosted.weblate.org/user/{{ contributor.weblate }}\" target=\"_blank\">\n"
"{%- endif -%}\n"
"{%- if contributor.Arab and site.data.scripts[site.active_lang] == \"Arab\" -%}\n"
"{{ contributor.Arab }}\n"
"{%- elsif contributor.Cyrl and site.data.scripts[site.active_lang] == \"Cyrl\" -%}\n"
"{{ contributor.Cyrl }}\n"
"{%- elsif contributor.Jpan and site.data.scripts[site.active_lang] == \"Jpan\" -%}\n"
"{{ contributor.Jpan }}\n"
"{%- elsif contributor.Kore and site.data.scripts[site.active_lang] == \"Kore\" -%}\n"
"{{ contributor.Kore }}\n"
"{%- elsif contributor.Hans and site.data.scripts[site.active_lang] == \"Hans\" -%}\n"
"{{ contributor.Hans }}\n"
"{%- elsif contributor.Hant and site.data.scripts[site.active_lang] == \"Hant\" -%}\n"
"{{ contributor.Hant }}\n"
"{%- else -%}\n"
"{{ contributor.name }}\n"
"{%- endif -%}\n"
"{%- if contributor.gitlab or contributor.weblate -%}\n"
"</a>\n"
"{%- endif -%}\n"
"</li>\n"
"{%- endfor -%}\n"
"</ul>\n"
msgstr ""
"<ul>\n"
"{%- for contributor in site.data.contributors -%}\n"
"<li>\n"
"{%- if contributor.gitlab -%}\n"
"<a href=\"https://gitlab.com/{{ contributor.gitlab }}\" target=\"_blank\">\n"
"{%- elsif contributor.weblate -%}\n"
"<a href=\"https://hosted.weblate.org/user/{{ contributor.weblate }}\" target=\"_blank\">\n"
"{%- endif -%}\n"
"{%- if contributor.Arab and site.data.scripts[site.active_lang] == \"Arab\" -%}\n"
"{{ contributor.Arab }}\n"
"{%- elsif contributor.Cyrl and site.data.scripts[site.active_lang] == \"Cyrl\" -%}\n"
"{{ contributor.Cyrl }}\n"
"{%- elsif contributor.Jpan and site.data.scripts[site.active_lang] == \"Jpan\" -%}\n"
"{{ contributor.Jpan }}\n"
"{%- elsif contributor.Kore and site.data.scripts[site.active_lang] == \"Kore\" -%}\n"
"{{ contributor.Kore }}\n"
"{%- elsif contributor.Hans and site.data.scripts[site.active_lang] == \"Hans\" -%}\n"
"{{ contributor.Hans }}\n"
"{%- elsif contributor.Hant and site.data.scripts[site.active_lang] == \"Hant\" -%}\n"
"{{ contributor.Hant }}\n"
"{%- else -%}\n"
"{{ contributor.name }}\n"
"{%- endif -%}\n"
"{%- if contributor.gitlab or contributor.weblate -%}\n"
"</a>\n"
"{%- endif -%}\n"
"</li>\n"
"{%- endfor -%}\n"
"</ul>\n"

#. type: Plain text
#: _pages/about.md
msgid "If you have contributed to F-Droid and your name is missing, it’s a mistake and you should [add yourself to the list](https://gitlab.com/fdroid/fdroid-website/blob/master/_data/contributors.yaml)! Please include your Weblate username if you are a translator.  Keep it sorted by surname, please."
msgstr ""

#. type: Plain text
#: _pages/about.md
msgid "The original F-Droid client app was based on the Aptoide app developed by Roberto Jacinto."
msgstr ""

#. type: YAML Front Matter: title
#: _pages/consulting.md
#, no-wrap
msgid "Consulting"
msgstr "পরামর্শ"

#. type: Plain text
#: _pages/consulting.md
msgid "If you're running an organisation that uses F-Droid or would like to use F-Droid, you will sometimes want to have an expert. This page is a list of companies and developers who contributed to F-Droid in one way or another and are available for consulting. By contracting one of these companies or developers, your organisation can also (indirectly) support the development of F-Droid."
msgstr ""

#. type: Plain text
#: _pages/consulting.md
#, no-wrap
msgid ""
"Keep in mind that the F-Droid ecosystem consists of several components. Thus, consulting services may offer a lot of different services, such as\n"
"  * Deployment of F-Droid within an organisation\n"
"  * Setup and maintenance of F-Droid repositories\n"
"  * Workshops for users and admins\n"
"  * Help in getting an app compliant for inclusion in the main f-droid.org repository\n"
"  * Bug fixes or feature additions to the F-Droid client and server components\n"
"  * Whitelabel solutions\n"
msgstr ""

#. type: Plain text
#: _pages/consulting.md
msgid "Check out the descriptions to find an appropriate consultant for your needs."
msgstr ""

#. type: Title ###
#: _pages/consulting.md
#, no-wrap
msgid "COTECH - Confidential Technologies GmbH"
msgstr "কোটেক - গোপনীয় প্রযুক্তি জিএমবিএইচ"

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Website:__ [https://www.cotech.de/f-droid](https://www.cotech.de/f-droid)"
msgstr "__ওয়েবসাইট__[https://www.cotech.de/f-droid](https://www.cotech.de/f-droid)"

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Email:__ [contact@cotech.de](mailto:contact@cotech.de)"
msgstr "__ইমেইল:__ [contact@cotech.de](mailto:contact@cotech.de)"

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Phone:__ +49 531 22435119"
msgstr "__ফোন:__ +৪৯ ৫৩১ ২২৪৩৫১১৯"

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Address:__ Confidential Technologies GmbH, Wilhelmsgarten 3, 38100 Braunschweig"
msgstr "__ঠিকানা:__ Confidential Technologies GmbH, Wilhelmsgarten 3, 38100 Braunschweig"

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Languages:__ English, German"
msgstr "__ ভাষা: __ ইংরেজি, জার্মান"

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Specializations:__ Custom F-Droid deployments, feature extensions, whitelabel solutions"
msgstr ""

#. type: Title ###
#: _pages/consulting.md
#, no-wrap
msgid "Hans-Christoph Steiner"
msgstr ""

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Website:__ [at.or.at](https://at.or.at)"
msgstr ""

#. type: Bullet: '* '
#: _pages/consulting.md
#, fuzzy
#| msgid "__Email:__ [info@izzysoft.de](mailto:info@izzysoft.de)"
msgid "__Email:__ [hans@eds.org](mailto:hans@eds.org)"
msgstr "__ইমেইল:__ [info@izzysoft.de](mailto:info@izzysoft.de)"

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Address:__ Vienna, Austria"
msgstr ""

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Specialization:__ Custom F-Droid deployments, reproducible builds, new features, whitelabel app stores, getting things merged."
msgstr ""

#. type: Title ###
#: _pages/consulting.md
#, no-wrap
msgid "IzzySoft"
msgstr "ইজিসফট"

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Website:__ [android.izzysoft.de](https://android.izzysoft.de/), [www.izzysoft.de](https://www.izzysoft.de/)"
msgstr "__ ওয়েবসাইট: __ [android.izzysoft.de](https://android.izzysoft.de/), [www.izzysoft.de](https://www.izzysoft.de/)"

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Email:__ [info@izzysoft.de](mailto:info@izzysoft.de)"
msgstr "__ইমেইল:__ [info@izzysoft.de](mailto:info@izzysoft.de)"

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Phone:__ +49 89 27373746"
msgstr "__ফোন:__ +৪৮ ৮৯ ২৭৩৭৩৭৪৬"

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Address:__ IzzySoft, 80796 München, Germany"
msgstr "__ ঠিকানা: __ ইজিসফট, ৮০৭৯৬ মেনচেন, জার্মানি"

#. type: Bullet: '* '
#: _pages/consulting.md
msgid "__Specialization:__ [Binary Repositories](https://f-droid.org/docs/Setup_an_F-Droid_App_Repo/) & Metadata (creator & operator of the [IzzyOnDroid Repo](https://apt.izzysoft.de/fdroid/))"
msgstr ""

#. type: Plain text
#: _pages/consulting.md
msgid "If you're missing, you can [add yourself to the list](https://gitlab.com/fdroid/fdroid-website/blob/master/_pages/consulting.md)! Keep it sorted alphabetically, please."
msgstr ""

#. type: YAML Front Matter: title
#: _pages/contribute.md
#, no-wrap
msgid "Contribute"
msgstr "অবদান রাখো"

#. type: Plain text
#: _pages/contribute.md
msgid "The project is entirely developed and maintained by volunteers. You can help:"
msgstr ""

#. type: Title ###
#: _pages/contribute.md
#, no-wrap
msgid "Report Problems"
msgstr "সমস্যাগুলোর প্রতিবেদন দাও"

#. type: Plain text
#: _pages/contribute.md
msgid "If you experience problems with the site or client software, you can report them in the issue tracker ([find the appropriate tracker on this page]({{ \"/issues/\" | prepend: site.baseurl }})), or discuss them in the [Forum](https://forum.f-droid.org/), on [Matrix](https://matrix.to/#/#fdroid:f-droid.org) or on IRC (#fdroid on OFTC)."
msgstr ""

#. type: Title ###
#: _pages/contribute.md
#, no-wrap
msgid "Submit Applications"
msgstr "অ্যাপ্লিকেশন জমা দাও"

#. type: Plain text
#: _pages/contribute.md
msgid "If you see an application missing from the repository (after reading the [inclusion policy](../docs/Inclusion_Policy)), please feel free to submit it via the dedicated [Requests For Packaging](https://gitlab.com/fdroid/rfp/issues) issue tracker."
msgstr ""

#. type: Plain text
#: _pages/contribute.md
msgid "If you have the technical skills required, you can also put together the relevant metadata and [submit that via the F-Droid Data repository](https://gitlab.com/fdroid/fdroiddata/blob/master/CONTRIBUTING.md), which will drastically speed up the inclusion of the application."
msgstr ""

#. type: Plain text
#: _pages/contribute.md
msgid "The same applies for helping to build newer versions of applications."
msgstr ""

#. type: Plain text
#: _pages/contribute.md
msgid "Further information can be found in [the documentation](../docs), or by asking on [Matrix](https://matrix.to/#/#fdroid:f-droid.org) or [IRC](https://webchat.oftc.net/?randomnick=1&channels=fdroid&prompt=1) (#fdroid on OFTC)."
msgstr ""

#. type: Title ###
#: _pages/contribute.md
#, no-wrap
msgid "Translate"
msgstr "অনুবাদ"

#. type: Plain text
#: _pages/contribute.md
msgid "The client application is available in many languages. Should yours not be among those, or be in need of updates or improvement, please create an account and use the [translation system](https://hosted.weblate.org/projects/f-droid/) to make your changes."
msgstr ""

#. type: Plain text
#: _pages/contribute.md
msgid "Start with the overview of [Translation and Localization](../docs/Translation_and_Localization).  There’s also a [dedicated forum section](https://forum.f-droid.org/c/translation) for translation related discussions."
msgstr ""

#. type: Title ###
#: _pages/contribute.md
#, no-wrap
msgid "Help with Development"
msgstr "উন্নয়নে সহায়তা করো"

#. type: Plain text
#: _pages/contribute.md
msgid "There are four git repositories hosted at GitLab – one for the [Android client application](https://gitlab.com/fdroid/fdroidclient), one for the [server tools](https://gitlab.com/fdroid/fdroidserver) for running a repository and building/installing applications locally, and one for the associated [metadata files for applications in the main F-Droid repository](https://gitlab.com/fdroid/fdroiddata).  The last repo is handling [website and documentation](https://gitlab.com/fdroid/fdroid-website).  The easiest way to contribute to development is to make clones of these projects and submit merge requests.  If you are making large changes, it would be good to discuss them on IRC or in the forum first, to ensure they fit with the direction of the project, and do not clash with or duplicate work already in development."
msgstr ""

#. type: Plain text
#: _pages/contribute.md
msgid "For working with the server and data projects, it’s a good idea to read [the manual]({{ \"/docs/\" | prepend: site.baseurl }})."
msgstr ""

#. type: Title ###
#: _pages/contribute.md
#, no-wrap
msgid "Help with Infrastructure"
msgstr "অবকাঠামোতে সহায়তা করো"

#. type: Plain text
#: _pages/contribute.md
msgid "F-Droid provides multiple servers (builders, web portal, download areas...) which need regular maintenance, Ansible-based deployment, and hardware management.  Further information can be found by asking on [Matrix](https://matrix.to/#/#fdroid-dev:f-droid.org), [IRC](https://webchat.oftc.net/?randomnick=1&channels=fdroid&prompt=1) (#fdroid-dev on OFTC) or team@f-droid.org."
msgstr ""

#. type: YAML Front Matter: title
#: _pages/docs.md
#, no-wrap
msgid "Docs"
msgstr "নির্দেশিকা"

#. type: Plain text
#: _pages/docs.md
msgid "F-Droid is both a repository of verified free software Android apps as well as a whole \"app store kit\", providing all the tools needed to setup and run an app store. It is a community-run free software project developed by a wide range of contributors. It also includes complete build and release tools for managing the process of turning app source code into published builds."
msgstr ""

#. type: Plain text
#: _pages/docs.md
msgid "The F-Droid server tools provide various scripts and tools that are used to maintain the main F-Droid application repository. You can use these same tools to create your own additional or alternative repository for publishing, or to assist in creating, testing and submitting metadata to the main repository."
msgstr ""

#. type: Title ###
#: _pages/docs.md
#, no-wrap
msgid "Getting Started"
msgstr "শুরু করা"

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[FAQ - General](FAQ_-_General) Frequently Asked Questions about F-Droid"
msgstr "[প্রাজিপ্র - সাধারণ](FAQ_-_General) F-Droid নিয়ে প্রায়শ জিজ্ঞাসিত প্রশ্ন"

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[FAQ - Client](FAQ_-_Client) Frequently Asked Questions about how to use the F-Droid app in Android."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Anti-Features](Anti-Features) What they are, what they mean, and what apps have them."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Tutorials](../tutorials) Step-by-step tutorials on features in F-Droid"
msgstr "[নির্দেশিকা](../tutorials) এফ-ড্রয়েডের বৈশিষ্টের উপর ধাপে ধাপে নির্দেশিকা"

#. type: Title ###
#: _pages/docs.md
#, no-wrap
msgid "More Information"
msgstr "আরো তথ্য"

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Code of Conduct](Code_of_Conduct) how to behave when communicating within F-Droid."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[How to Help](How_to_Help) Different ways anyone can contribute to the F-Droid project."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Known Repositories](https://forum.f-droid.org/t/721) A list that tries to keep track of known F-Droid compatible repositories."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Release Channels and Signing Keys](Release_Channels_and_Signing_Keys) These are the various channels that F-Droid software are released on, with info to verify them based on signing keys."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Security Model](Security_Model) A brief explanation of how F-Droid delivers software to users securely."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Repomaker](../repomaker) A user-friendly tool for managing custom repos"
msgstr "[রিপোমেকার](../repomaker) রিপো/ভাণ্ডার বানানোর জন্য সহজে ব্যবহারযোগ্য সরঞ্জাম"

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Translation and Localization](Translation_and_Localization) How all the parts of F-Droid are localized."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Tips & Tricks](https://forum.f-droid.org/c/wiki/tips-and-tricks) A collection of useful tips and tricks that make life around F-Droid easier."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[All our APIs](All_our_APIs) Public API endpoints for data about _f-droid.org_, repos, mirrors, Android, Gradle, and more"
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Wiki](https://gitlab.com/fdroid/wiki/-/wikis/home) For everything else that people want to document."
msgstr "[উইকি](https://gitlab.com/fdroid/wiki/-/wikis/home) কেউ মনে করে যে নির্দেশিকায় এটা থাকা উচিত, সেসব এখানে।"

#. type: Title ###
#: _pages/docs.md
#, no-wrap
msgid "Developers"
msgstr "ডেভেলপার"

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[FAQ - App Developers](FAQ_-_App_Developers) Frequently Asked Questions about F-Droid"
msgstr "[প্রাজিপ্র- অ্যাপ তৈরিকারক](FAQ_-_App_Developers) এফ-ড্রয়েড নিয়ে প্রায়শ জিজ্ঞাসিত প্রশ্ন"

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Build Metadata Reference](Build_Metadata_Reference) All about the build \"recipes\""
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Submitting to F-Droid: Quick Start Guide](Submitting_to_F-Droid_Quick_Start_Guide) Add an app to f-droid.org"
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Update Processing](Update_Processing) How updates get detected and added."
msgstr "[হালনাগাদ প্রক্রিয়া](Update_Processing) কিভাবে হালনাগাদ শনাক্ত ও যোগ করা হয়।"

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Inclusion Policy](Inclusion_Policy) & [Inclusion How-To](Inclusion_How-To)  Guidelines for how to request the inclusion of a new app."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Importing Applications](Importing_Applications) Using `fdroid import` to build a new project."
msgstr "[অ্যাপলিকেশন আমদানিকরণ](Importing_Applications) `fdroid import` দিয়ে নতুন প্রকল্প বানানো।"

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Building Applications](Building_Applications) Using `fdroid build` to build an app."
msgstr "[অ্যাপলিকেশন বানানো](Building_Applications) `fdroid build` একটি অ্যাপ বানানো।"

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[All About Descriptions, Graphics, and Screenshots](All_About_Descriptions_Graphics_and_Screenshots) Showing off your app in F-Droid."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Publishing Nightly Builds](Publishing_Nightly_Builds) Make an automated repo of nightly builds of your app."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Installing the Server and Repo Tools](Installing_the_Server_and_Repo_Tools) How to install fdroidserver, the tools for running repos and build servers."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Setup an F-Droid App Repo](Setup_an_F-Droid_App_Repo) How to setup your own repository of apps that users can easily add to their F-Droid client to get your apps directly from the source."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Build Server Setup](Build_Server_Setup) How to setup the complete f-droid.org build setup on your own machine."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Signing Process](Signing_Process) How to setup cryptographic signatures with F-Droid tools"
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Reproducible Builds](Reproducible_Builds) How to use the F-Droid tools to make reproducible builds easy."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Building a Signing Server](Building_a_Signing_Server) For higher security, isolate the signing keys from the rest."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Verification Server](Verification_Server) Setting up a server to verify app builds are correct."
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Whitelabel Builds](Whitelabel_Builds) custom builds of F-Droid"
msgstr "[মোড়ক ছাড়া নির্মাণ](Whitelabel_Builds) এফ-ড্রয়েডের বিশেষ নির্মাণ"

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Privileged Extension](https://gitlab.com/fdroid/privileged-extension/#f-droid-privileged-extension) How to use F-Droid Privileged Extension as a user and rom developer"
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Deploying the Website](Deploying_the_Website) How [fdroid-website.git](https://gitlab.com/fdroid/fdroid-website) becomes [f-droid.org](https://f-droid.org)"
msgstr "[ওয়েবসাইট শুরু করা হচ্ছে](Deploying_the_Website) কিভাবে [fdroid-website.git](https://gitlab.com/fdroid/fdroid-website) থেকে হয় [f-droid.org](https://f-droid.org)"

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Maintaining the Forum](Maintaining_the_Forum) Useful information for administrators of [F-Droid's forum](https://forum.f-droid.org)"
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[Running a Mirror](Running_a_Mirror) Instructions for setting up a repository mirror"
msgstr ""

#. type: Bullet: '* '
#: _pages/docs.md
msgid "[MIA Process](MIA_Process) MIA (missing in action) maintainer removal process"
msgstr ""

#. type: YAML Front Matter: title
#: _pages/donate.md
#, no-wrap
msgid "Donations"
msgstr "অনুদান"

#. type: Plain text
#: _pages/donate.md
msgid "F-Droid is powered by your donations!"
msgstr "তোমাদের অনুদানেই এফ-ড্রয়েড চলে!"

#. type: Plain text
#: _pages/donate.md
msgid "If you want to contribute financially, we recommend using a **free (as in free software) donation platform** like **[Liberapay](https://liberapay.com/F-Droid-Data/)** or **[Open Collective](https://opencollective.com/f-droid/)**. **Liberapay** has lower fees but **Open Collective** allows more flexibility and transparency on how the funds are used.  Both platforms allow for recurrent donations which are highly desirable for making F-Droid sustainable."
msgstr ""

#. type: Plain text
#: _pages/donate.md
#, no-wrap
msgid ""
"<p align=\"center\">\n"
"<a href=\"https://opencollective.com/f-droid\"><img src=\"{% asset opencollective_button.png %}\" height=\"50\" style=\"box-shadow: unset;\" ></a>\n"
"&nbsp; &nbsp; &nbsp;\n"
"<a href=\"https://liberapay.com/F-Droid-Data/\"><img src=\"{% asset liberapay_donate_button.svg %}\" height=\"50\" style=\"box-shadow: unset;\" ></a>\n"
"</p>\n"
msgstr ""

#. type: Title ####
#: _pages/donate.md
#, no-wrap
msgid "Other options"
msgstr "অন্যান্য অপশন"

#. type: Plain text
#: _pages/donate.md
msgid "You can donate to F-Droid Limited (which runs the core infrastructure) directly via:"
msgstr ""

#. type: Bullet: '* '
#: _pages/donate.md
msgid "[![Donate via Bitcoin]({% asset bitcoin.png %})](https://blockchain.info/address/15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18): [`15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18`](bitcoin:15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18)"
msgstr "[![বিটকয়েন দিয়ে অনুদান দাও]({% asset bitcoin.png %})](https://blockchain.info/address/15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18): [`15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18`](bitcoin:15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18)"

#. type: Bullet: '* '
#: _pages/donate.md
msgid "[Donate via PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=E2FCXCT6837GL)"
msgstr "[পেপাল এর মাধ্যমে দান করো](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=E2FCXCT6837GL)"

#. type: Bullet: '* '
#: _pages/donate.md
msgid "[Donate via Flattr](https://flattr.com/thing/343053/F-Droid-Repository)"
msgstr "[ফ্ল্যাটারের মাধ্যমে দান করো](https://flattr.com/thing/343053/F-Droid-Repository)"

#. type: Plain text
#: _pages/donate.md
msgid "__Bank Transfer__"
msgstr "__ব্যাংক ট্রান্সফার__"

#. type: Fenced code block (nohighlight)
#: _pages/donate.md
#, no-wrap
msgid ""
"Name: F-Droid Limited\n"
"IBAN: GB92 BARC 2056 7893 4088 92\n"
"SWIFT: BARCGB22\n"
msgstr ""

#. type: Plain text
#: _pages/donate.md
#, no-wrap
msgid ""
"Or buy a 👕 T-shirt 👕 from <br>\n"
"[![HELLOTUX]({% asset hellotux_banner.jpg %})](https://www.hellotux.com/f-droid)<br>\n"
"(F-Droid will receive 3€ per shirt sold.)\n"
msgstr ""

#. type: YAML Front Matter: title
#: _pages/issues.md
#, no-wrap
msgid "Issues"
msgstr "সমস্যা"

#. type: Plain text
#: _pages/issues.md
#, no-wrap
msgid ""
"* [Client](https://gitlab.com/fdroid/fdroidclient/issues) issue tracker: If you’re having a problem with the F-Droid client application,\n"
"please check if we already know about it, and/or report it in this issue tracker.\n"
msgstr ""

#. type: Plain text
#: _pages/issues.md
#, no-wrap
msgid ""
"* [RFP](https://gitlab.com/fdroid/rfp/issues/) tracker: To request a new app be added, submit a \"Request For Packaging\" on the\n"
"rfp tracker.\n"
msgstr ""

#. type: Plain text
#: _pages/issues.md
#, no-wrap
msgid ""
"* [Data](https://gitlab.com/fdroid/fdroiddata/issues) issue tracker: For problems relating to the contents of the repository,\n"
"such as missing or outdated applications, please use this issue tracker.\n"
msgstr ""

#. type: Plain text
#: _pages/issues.md
#, no-wrap
msgid ""
"* [Server](https://gitlab.com/fdroid/fdroidserver/issues) issue tracker: For the server tools (used for building apps yourself,\n"
"or running your own repo), please use this issue tracker.\n"
msgstr ""

#. type: Plain text
#: _pages/issues.md
#, no-wrap
msgid ""
"* [Website](https://gitlab.com/fdroid/fdroid-website/issues) issue tracker: For issues relating to this web site, you can use\n"
"this issue tracker.\n"
msgstr ""

#. type: Plain text
#: _pages/issues.md
#, no-wrap
msgid ""
"* [Website-Search](https://gitlab.com/fdroid/fdroid-website-search/issues) issue tracker: For issues relating to\n"
"https://search.f-droid.org, you can use this issue tracker.\n"
msgstr ""

#. type: YAML Front Matter: title
#: _pages/repomaker.md
#, no-wrap
msgid "Repomaker"
msgstr "রিপোমেকার"

#. type: Plain text
#: _pages/repomaker.md
msgid "F-Droid.org is just a repo out of hundreds of repos created by individuals all around the globe. With the tools of F-Droid, everyone can create their own repo. So whether you are a musician who wants to publish their music or a developer who wants to serve nightly builds of their app, you are free to create your own repo and share it with other people independently of F-Droid.org."
msgstr ""

#. type: Plain text
#: _pages/repomaker.md
msgid "In the past, creating a repo has been difficult because you had to have knowledge on the command line, needed to edit text files to edit your packages' store details and had to paste screenshots in a special system of directories to have them served well inside the F-Droid app."
msgstr ""

#. type: Plain text
#: _pages/repomaker.md
msgid "This all got easier now: with Repomaker, you are able to create your own repo and do not need to have any special knowledge to do so.  Check out the [installation guide](https://gitlab.com/fdroid/repomaker/blob/master/README.md#installation)  for information on how you can install Repomaker on your device."
msgstr ""

#. type: Plain text
#: _pages/repomaker.md
#, no-wrap
msgid ""
"[![]({% asset repomaker-screenshots/repo-details.png %})]({% asset repomaker-screenshots/repo-details.png %})<br/>\n"
"*Your repo can serve all types of media*\n"
msgstr ""

#. type: Plain text
#: _pages/repomaker.md
#, no-wrap
msgid ""
"[![]({% asset repomaker-screenshots/package-details.png %})]({% asset repomaker-screenshots/package-details.png %})<br/>\n"
"*Editing your package store details never was easier*\n"
msgstr ""

#. type: Plain text
#: _pages/repomaker.md
#, no-wrap
msgid ""
"[![]({% asset repomaker-screenshots/create-repo.png %})]({% asset repomaker-screenshots/create-repo.png %})<br/>\n"
"*Creating a repo just takes you two clicks*\n"
msgstr ""

#. type: Plain text
#: _pages/repomaker.md
#, no-wrap
msgid ""
"[![]({% asset repomaker-screenshots/add-storage.png %})]({% asset repomaker-screenshots/add-storage.png %})<br/>\n"
"*You have many possibilities where to upload your repo*\n"
msgstr ""

#. type: Plain text
#: _pages/repomaker.md
msgid "Like any other project of F-Droid, Repomaker is free and open source software. You can find the source code and its license on [gitlab.com](https://gitlab.com/fdroid/repomaker)."
msgstr "এফ-ড্রয়েডের অন্য সকল প্রকল্পের মতোই, রিপোমেকার ফ্রি ও মুক্ত উৎসবিশিষ্ট সফ্টওয়্যার যার অনুমতিপত্র আছে [gitlab.com](https://gitlab.com/fdroid/repomaker) এ।"
